(asdf:defsystem :com.informatimago.lse
    :description  "An implementation of ISLISP Working Draft 23.0"
    :author "<PJB> Pascal J. Bourguignon <pjb@informatimago.com>"
    :version "1.0.0"
    :licence "AGPL3"
    :properties ((#:author-email                   . "pjb@informatimago.com")
                 (#:date                           . "Summer 2019")
                 ((#:albert #:output-dir)          . "/tmp/documentation/com.informatimago.islisp/")
                 ((#:albert #:formats)             . ("docbook"))
                 ((#:albert #:docbook #:template)  . "book")
                 ((#:albert #:docbook #:bgcolor)   . "white")
                 ((#:albert #:docbook #:textcolor) . "black"))

    #+asdf-unicode :encoding #+asdf-unicode :utf-8

    :depends-on (
                 ;; "split-sequence"
                 ;; "alexandria"
                 ;; "babel"
                 ;; 
                 ;; "com.informatimago.common-lisp"
                 ;; "com.informatimago.rdp"

                 )

    :components (
                 ;; Some generic utility
                 (:file "packages")
                 ))
